# README
This is update of Shadowsocks-libQSS project.

## Issues
### Botan 1.10 Install problem in Windows
You should use following option when install botan-1.10 with python.

    --link-method=copy

That is,

        $ python configure.py --cc=msvc --os=windows --link-method=copy
        $ nmake
        $ botan-test.exe
        $ nmake install
    
You should run this command in only VS2015 x64 Native Tools Command Prompt.
(Don't use VS2015 x64 x86 Cross Tools Command Prompt - it makes some terrible error, such as '_umul128': identifier not found)

### Botan 2.3.0 Install in Ubuntu
You should use following command when install botan-2.3.0 in Ubuntu.

    $ python configure.py 
    $ make
    $ make install

### Qt5::Core, Qt5::Network package Install in Ubuntu
You should use following command to install Qt5 package (including Qt5::Core, Qt5::Network)

    sudo apt install qtbase5-dev libqt5location5 qtdeclarative5-controls-plugin
    
### Qt5 Creator Install in Ubuntu

    sudo apt-get install build-essential
    sudo apt-get install qtcreator
    sudo apt-get install qt5-default